SELECT * FROM Customers;

SELECT * FROM Customers WHERE Country='Mexico';

SELECT * FROM Customers ORDER BY Country;

INSERT INTO Customers (CustomerName, ContactName, Address, City, PostalCode, Country)
VALUES ('Cardinal', 'Tom B. Erichsen', 'Skagen 21', 'Stavanger', '4006', 'Norway');

SELECT * FROM Customers WHERE Country='Germany' AND City='Berlin';

SELECT * FROM Customers WHERE City='Berlin' OR City='München';

SELECT * FROM Customers WHERE NOT Country='Germany';

SELECT CustomerName, ContactName, Address FROM Customers WHERE Address IS NULL;

SELECT CustomerName, ContactName, Address FROM Customers WHERE Address IS NOT NULL;